//Load express
var express = require("express");

//Create an instance of the express application
var app = express();
var routes = require("./routes");
var bodyParser = require("body-parser");
var compression = require('compression');

const PORT = process.env.PORT;

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(compression())
// Run functions in route.js
routes.init(app);
routes.errorHandler(app);

//Start the web server on port 3000
app.listen(PORT, function() {
    console.info(`App Server started on port ${PORT}`);
    console.info(__dirname);
});